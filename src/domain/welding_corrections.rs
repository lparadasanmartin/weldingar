// ----------------------​----------------------​----------------------​----------------------​----------------------​
// ----------------------​----------------------​----------------------​----------------------​----------------------​
//                                    Domain - Welding corrections
// ----------------------​----------------------​----------------------​----------------------​----------------------​
// ----------------------​----------------------​----------------------​----------------------​----------------------​

use decimal_percentage::Percentage;
use std::collections::VecDeque;

// Welding correction time_base hardcoded, could be done differently
// This behaviour remains isolated in the infrastructure layer
pub const WELDING_CORRECTION_TIME_BASE: f32 = 1000.0;

// ----------------------------------------------------------
// ----------------------- Structures -----------------------
// ----------------------------------------------------------

#[derive(Debug)]
pub struct WeldingCorrection {
    pub time_seconds: f32,
    pub v_pulse: Pulse,
    pub y_pulse: Pulse,
    pub z_pulse: Pulse,
    pub progression: WeldingCompletion,
}

#[derive(Debug)]
pub struct WeldingCorrections {
    pub corrections: VecDeque<WeldingCorrection>,
}

#[derive(Debug)]
pub struct WeldingCompletion(pub Percentage);

#[derive(Debug)]
pub enum Pulse {
    None,
    Positive,
    Negative,
}
