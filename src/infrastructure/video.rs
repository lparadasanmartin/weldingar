// ----------------------​----------------------​----------------------​----------------------​----------------------​
// ----------------------​----------------------​----------------------​----------------------​----------------------​
//                                    Infrastructure - Raw video decoders
// ----------------------​----------------------​----------------------​----------------------​----------------------​
// ----------------------​----------------------​----------------------​----------------------​----------------------​

use crate::domain::welding_video::*;
use std::path::PathBuf;
extern crate ffmpeg_next as ffmpeg;

pub fn read_video_characteristics_from_path(
    path_buf: PathBuf,
) -> Result<Option<WeldingVideoCharacteristics>, ffmpeg::Error> {
    let mut welding_video_characteristics: Option<WeldingVideoCharacteristics> = None;
    match ffmpeg::format::input(&path_buf) {
        Ok(context) => {
            for stream in context.streams() {
                // Only one stream expected, even though any stream would be ok for what we are looking for
                if stream.index() == 0 {
                    let codec =
                        ffmpeg::codec::context::Context::from_parameters(stream.parameters())?;
                    if codec.medium() == ffmpeg::media::Type::Video {
                        if let Ok(video) = codec.decoder().video() {
                            welding_video_characteristics = Some(WeldingVideoCharacteristics {
                                duration_seconds: (context.duration() as f64
                                    / f64::from(ffmpeg::ffi::AV_TIME_BASE)),
                                frames_per_second: stream.time_base().denominator(),
                                number_of_frames: stream.frames(),
                                width: video.width(),
                                height: video.height(),
                            })
                        }
                    }
                }
            }
            println!(
                "welding_video_characteristics : {:?}",
                welding_video_characteristics
            );
            Ok(welding_video_characteristics)
        }
        Err(error) => Err(error),
    }
}

pub fn _print_ffmpeg_data(path_buf: PathBuf) -> Result<(), ffmpeg::Error> {
    match ffmpeg::format::input(&path_buf) {
        Ok(context) => {
            for (k, v) in context.metadata().iter() {
                println!("{}: {}", k, v);
            }

            if let Some(stream) = context.streams().best(ffmpeg::media::Type::Video) {
                println!("Best video stream index: {}", stream.index());
            }

            if let Some(stream) = context.streams().best(ffmpeg::media::Type::Audio) {
                println!("Best audio stream index: {}", stream.index());
            }

            if let Some(stream) = context.streams().best(ffmpeg::media::Type::Subtitle) {
                println!("Best subtitle stream index: {}", stream.index());
            }

            println!(
                "duration (seconds): {:.2}",
                context.duration() as f64 / f64::from(ffmpeg::ffi::AV_TIME_BASE)
            );

            for stream in context.streams() {
                println!("stream index {}:", stream.index());
                println!("\ttime_base: {}", stream.time_base());
                println!("\tstart_time: {}", stream.start_time());
                println!("\tduration (stream timebase): {}", stream.duration());
                println!(
                    "\tduration (seconds): {:.2}",
                    stream.duration() as f64 * f64::from(stream.time_base())
                );
                println!("\tframes: {}", stream.frames());
                println!("\tdisposition: {:?}", stream.disposition());
                println!("\tdiscard: {:?}", stream.discard());
                println!("\trate: {}", stream.rate());

                let codec = ffmpeg::codec::context::Context::from_parameters(stream.parameters())?;
                println!("\tmedium: {:?}", codec.medium());
                println!("\tid: {:?}", codec.id());

                if codec.medium() == ffmpeg::media::Type::Video {
                    if let Ok(video) = codec.decoder().video() {
                        println!("\tbit_rate: {}", video.bit_rate());
                        println!("\tmax_rate: {}", video.max_bit_rate());
                        println!("\tdelay: {}", video.delay());
                        println!("\tvideo.width: {}", video.width());
                        println!("\tvideo.height: {}", video.height());
                        println!("\tvideo.format: {:?}", video.format());
                        println!("\tvideo.has_b_frames: {}", video.has_b_frames());
                        println!("\tvideo.aspect_ratio: {}", video.aspect_ratio());
                        println!("\tvideo.color_space: {:?}", video.color_space());
                        println!("\tvideo.color_range: {:?}", video.color_range());
                        println!("\tvideo.color_primaries: {:?}", video.color_primaries());
                        println!(
                            "\tvideo.color_transfer_characteristic: {:?}",
                            video.color_transfer_characteristic()
                        );
                        println!("\tvideo.chroma_location: {:?}", video.chroma_location());
                        println!("\tvideo.references: {}", video.references());
                        println!("\tvideo.intra_dc_precision: {}", video.intra_dc_precision());
                    }
                } else if codec.medium() == ffmpeg::media::Type::Audio {
                    if let Ok(audio) = codec.decoder().audio() {
                        println!("\tbit_rate: {}", audio.bit_rate());
                        println!("\tmax_rate: {}", audio.max_bit_rate());
                        println!("\tdelay: {}", audio.delay());
                        println!("\taudio.rate: {}", audio.rate());
                        println!("\taudio.channels: {}", audio.channels());
                        println!("\taudio.format: {:?}", audio.format());
                        println!("\taudio.frames: {}", audio.frames());
                        println!("\taudio.align: {}", audio.align());
                        println!("\taudio.channel_layout: {:?}", audio.channel_layout());
                    }
                }
            }
        }

        Err(error) => println!("error: {}", error),
    }
    Ok(())
}
