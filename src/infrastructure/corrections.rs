// ----------------------​----------------------​----------------------​----------------------​----------------------​
// ----------------------​----------------------​----------------------​----------------------​----------------------​
//                                    Infrastructure - Reading csv files
// ----------------------​----------------------​----------------------​----------------------​----------------------​
// ----------------------​----------------------​----------------------​----------------------​----------------------​

use crate::domain::welding_corrections::*;
use csv::ReaderBuilder;
use decimal_percentage::Percentage;
use serde::Deserialize;
use std::{error::Error, path::PathBuf};

// Reading correction from csv
pub fn read_corrections_from_path(csv_path: PathBuf) -> Result<WeldingCorrections, Box<dyn Error>> {
    let mut reader = ReaderBuilder::new()
        .has_headers(true)
        .delimiter(b';')
        .from_path(csv_path)?;
    let iter = reader.deserialize::<UnverifiedCorrection>();

    // Creating verified corrections vector
    let mut welding_corrections_vec = WeldingCorrections::new();
    // let mut current_welding_correction: Option<WeldingCorrection> = None;

    // Iterating through csv reader
    for row_result in iter {
        match row_result {
            Ok(unverified_row) => {
                let unverified_correction: UnverifiedCorrection = unverified_row;
                // Verifying row validity
                match WeldingCorrection::option_from(unverified_correction) {
                    // Valid row ! Taking it into account as welding correction
                    Some(welding_correction) => {
                        welding_corrections_vec.push_back(welding_correction);
                    }
                    None => {
                        println!("Incompatible correction detected while decoding correction csv");
                    }
                }
            }
            Err(e) => {
                println!("Error while read_corrections_from_path : {}", e);
                return Err(From::from("Error while read_corrections_from_path"));
            }
        }
    }
    Ok(welding_corrections_vec)
}

#[derive(Debug, Deserialize)]
pub struct UnverifiedCorrection {
    #[serde(rename = "TIME")]
    pub time: f32,
    #[serde(rename = "V_PULSE")]
    pub v_pulse: i8,
    #[serde(rename = "Y_PULSE")]
    pub y_pulse: i8,
    #[serde(rename = "Z_PULSE")]
    pub z_pulse: i8,
    #[serde(rename = "CURRENT_SEGMENT_INDEX")]
    pub _current_segment_index: i8,
    #[serde(rename = "PROGRESS_RATIO")]
    pub progress_ratio: f32,
}

impl WeldingCorrection {
    fn option_from(unverified_correction: UnverifiedCorrection) -> Option<Self> {
        match (
            Pulse::option_from(unverified_correction.v_pulse),
            Pulse::option_from(unverified_correction.y_pulse),
            Pulse::option_from(unverified_correction.z_pulse),
            WeldingCompletion::option_from(unverified_correction.progress_ratio),
        ) {
            (Some(v_pulse), Some(y_pulse), Some(z_pulse), Some(progression)) => {
                Some(WeldingCorrection {
                    time_seconds: unverified_correction.time / WELDING_CORRECTION_TIME_BASE,
                    v_pulse,
                    y_pulse,
                    z_pulse,
                    progression,
                })
            }
            _other => None,
        }
    }
}

impl Pulse {
    fn option_from(integer: i8) -> Option<Self> {
        match integer {
            0 => Some(Pulse::None),
            1 => Some(Pulse::Positive),
            -1 => Some(Pulse::Negative),
            _other => None,
        }
    }
}

impl WeldingCompletion {
    fn option_from(float: f32) -> Option<Self> {
        if float > 0.0 && float < 100.0 {
            Some(WeldingCompletion(Percentage::from(float)))
        } else {
            None
        }
    }
}
