// ----------------------​----------------------​----------------------​----------------------​----------------------​
// ----------------------​----------------------​----------------------​----------------------​----------------------​
//                                    Infrastructure - Command line arguments
// ----------------------​----------------------​----------------------​----------------------​----------------------​
// ----------------------​----------------------​----------------------​----------------------​----------------------​

use std::path::PathBuf;
use walkdir::{DirEntry, WalkDir};

// Getting the video and the corrections csv files path contained in folder
pub fn get_files_path() -> Option<((PathBuf, String), PathBuf)> {
    let mut video_file_path_option: Option<PathBuf> = None;
    let mut video_file_name: String = String::from("");
    let mut csv_file_path_option: Option<PathBuf> = None;
    match arguments::parse(std::env::args()) {
        Ok(arguments) => {
            // Arguments found, looking for folder argument only so far
            match arguments.get::<String>("folder") {
                Some(folder_path_argument) => {
                    // Argument folder path found
                    let folder_path = PathBuf::from(&folder_path_argument);
                    // Looking for the video file path
                    for entry_result in WalkDir::new(folder_path) {
                        match entry_result {
                            Ok(entry) => {
                                if is_video(&entry) {
                                    video_file_path_option = Some(entry.path().to_path_buf());
                                    video_file_name = String::from(
                                        entry.path().file_name().unwrap().to_str().unwrap(),
                                    );
                                }
                                if is_csv(&entry) {
                                    csv_file_path_option = Some(entry.path().to_path_buf());
                                }
                            }
                            Err(e) => {
                                println!("unexpected error while reading folder : {}", e);
                            }
                        }
                    }
                }
                None => {
                    // Argument folder path not found
                    println!("no folder provided");
                }
            }
        }
        Err(e) => {
            println!("unexpected error while reading arguments : {}", e);
        }
    }

    // Returning complete tuple or nothing
    if let (Some(video_file_path), Some(csv_file_path)) =
        (video_file_path_option, csv_file_path_option)
    {
        Some(((video_file_path, video_file_name), csv_file_path))
    } else {
        None
    }
}

// Looking only for AVI files for the moment
fn is_video(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|file_name| file_name.ends_with("avi"))
        .unwrap_or(false)
}

fn is_csv(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|file_name| file_name.ends_with("csv"))
        .unwrap_or(false)
}
