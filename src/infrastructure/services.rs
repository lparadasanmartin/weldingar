// ----------------------​----------------------​----------------------​----------------------​----------------------​
// ----------------------​----------------------​----------------------​----------------------​----------------------​
//                                    Infrastructure - Services interfaces
// ----------------------​----------------------​----------------------​----------------------​----------------------​
// ----------------------​----------------------​----------------------​----------------------​----------------------​

use super::corrections::*;
use super::video::*;
use std::path::PathBuf;
use video_rs::{self, Decoder};
use video_rs::{Encoder, EncoderSettings, Locator};

use crate::domain::welding_ar::WeldingAR;
use crate::infrastructure::command_line_arguments::*;
extern crate ffmpeg_next as ffmpeg;

pub fn apply_corrections_and_ar_to_welding_video() {
    // Starting only if correct arguments given and required files found
    if let Some(((source_video_path, source_video_file_name), corrections_csv_path)) =
        get_files_path()
    {
        // Initializing video library
        video_rs::init().unwrap();

        // ------------------------------
        // Welding corrections management
        // ------------------------------
        let mut welding_corrections = read_corrections_from_path(corrections_csv_path)
            .expect("failed to read_corrections_from_path");

        // Applying Homothesie
        if let Ok(Some(video_characteristics)) =
            read_video_characteristics_from_path(source_video_path.clone())
        {
            welding_corrections.apply_homothesie(video_characteristics);
        } else {
            println!("WARNING : REQUIRED VIDEO CHARACTERISTICS NOT FOUND FOR HOMOTHESIE COMPUTATION, USING RAW WELDING CORRECTIONS");
        }

        // Getting first welding correction for algorithm
        let (mut next_welding_correction_option, mut next_welding_correction_time_option) =
            welding_corrections.get_next_correction();
        println!(
            "first correction time : {:?}",
            next_welding_correction_time_option
        );

        // ------------------------------
        // Video management
        // ------------------------------
        let mut source_video_decoder =
            Decoder::new(&source_video_path.clone().into()).expect("failed to create decoder");

        // Destination video
        let destination_video: Locator =
            PathBuf::from("AR".to_owned() + &source_video_file_name + ".mp4").into();
        let destination_video_settings = EncoderSettings::for_h264_yuv420p(1000, 600, false);
        let mut destination_video_encoder =
            Encoder::new(&destination_video, destination_video_settings)
                .expect("failed to create encoder");

        // ------------------------------
        // AR management
        // ------------------------------
        let welding_ar = WeldingAR::new();

        // ------------------------------
        // Performing service
        // ------------------------------

        // Looping through the source video
        for decoder_iterator in source_video_decoder.decode_iter() {
            if let Ok((source_time, source_frame)) = decoder_iterator {
                if next_welding_correction_time_option.is_some()
                    && Some(source_time.as_secs()) > next_welding_correction_time_option
                {
                    println!(
                        "Encoding video with augmented reality at : {:?}",
                        source_time.as_secs()
                    );
                    // Encode video with augmented reality
                    let welding_ar_frame = welding_ar
                        .get_welding_ar_frame(source_frame, next_welding_correction_option);
                    destination_video_encoder
                        .encode(welding_ar_frame, &source_time)
                        .expect("failed to encode frame");
                    // Getting next correction
                    (
                        next_welding_correction_option,
                        next_welding_correction_time_option,
                    ) = welding_corrections.get_next_correction();
                } else {
                    // Encode video without any augmented reality
                    println!("Original video : {:?}", source_time.as_secs());
                    // println!("Original video frame : {}", source_frame);
                    match destination_video_encoder.encode(&source_frame, &source_time) {
                        Ok(_) => {}
                        Err(e) => {
                            println!("Error while encoding: {}", e);
                        }
                    }
                }
            } else {
                break;
            }
        }
        destination_video_encoder
            .finish()
            .expect("failed to finish encoder");
    } else {
        println!("1 Argument expected: .\\welding_ar --folder \"folder containing data\"");
    }
}
