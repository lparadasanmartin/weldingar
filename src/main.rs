mod domain;
mod infrastructure;
mod usecases;

use crate::infrastructure::services::*;

fn main() {
    // Processing unique program service
    apply_corrections_and_ar_to_welding_video();
}
