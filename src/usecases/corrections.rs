// ----------------------​----------------------​----------------------​----------------------​----------------------​
// ----------------------​----------------------​----------------------​----------------------​----------------------​
//                                    Use Case - Corrections
// ----------------------​----------------------​----------------------​----------------------​----------------------​
// ----------------------​----------------------​----------------------​----------------------​----------------------​

use crate::domain::{welding_corrections::*, welding_video::WeldingVideoCharacteristics};
use std::collections::VecDeque;

impl WeldingCorrections {
    pub fn new() -> WeldingCorrections {
        WeldingCorrections {
            corrections: VecDeque::<WeldingCorrection>::new(),
        }
    }

    pub fn push_back(&mut self, verified_correction: WeldingCorrection) {
        self.corrections.push_back(verified_correction)
    }

    pub fn get_next_correction(&mut self) -> (Option<WeldingCorrection>, Option<f32>) {
        if let Some(next_correction) = self.corrections.pop_front() {
            let welding_correction_time = next_correction.time_seconds;
            (Some(next_correction), Some(welding_correction_time))
        } else {
            (None, None)
        }
    }

    pub fn apply_homothesie(&mut self, video_characteristics: WeldingVideoCharacteristics) {
        // Getting last element in the welding correction (if any)
        if let Some(last_correction) = self.corrections.back() {
            // Getting data for computation
            let last_correction_time = &last_correction.time_seconds;
            let last_correction_progression = &last_correction.progression;
            let video_duration_seconds = video_characteristics.duration_seconds;
            // Computing homothesie
            let last_correction_real_time_on_video =
                (video_duration_seconds * last_correction_progression.0) as f32;
            let homothesie_to_apply = last_correction_real_time_on_video / last_correction_time;
            println!("homothesie_to_apply: {}", homothesie_to_apply);
            // Applying homothesie
            for correction in self.corrections.iter_mut() {
                correction.time_seconds *= homothesie_to_apply;
            }
        }
    }
}
