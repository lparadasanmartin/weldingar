# WeldingAR

This program aims at adding augmented reality information on the video given in entry (the "welding source" video) with "welding corrections" applied during the video capture.

**BEWARE: So far this version of the program only adds a "debug frame" (a frame with unified color) at the moment of the applied correction.**

The main interest of this program is to show the capacity of an **efficient and safe functional capable language** (rust) with a **Domain Driven Design** approach implemented on a **Onion architecture**. Allowing robust/efficient (rust) easy/maintainable separation of concerns (ddd & onion architecture) for future evolution of the code.

As an example, in a further version the "debug frame" replacement for the real "augmented reality frame" will only impact the functional layer (usecases), namely the **get_welding_ar_frame** function. Not the domain itself or the way data was acquired.

# Interesting resources on the matter

Domain Driven Design = https://youtu.be/PLFl95c-IiU?si=bs3xputr_QyRF9zh

Scott Wlaschin's book is also awesome !

## Getting started

```
cd destination_repo_for_ar_augmented_videos
./welding_ar --folder "source_folder_containing_video_and_corrections_csv"
```

## License
Open source project